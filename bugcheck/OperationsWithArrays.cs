using System;

namespace bugcheck
{
    public enum SortOrder { Ascending, Descending };
    static public class OperationsWithArrays
    {
        /// <summary>
        /// Should sort array in SortOrder order
        /// <exception cref="ArgumentNullException">Thrown when wrong parameter array</exception>
        /// </summary>

        public static void SortArray(int[] array, SortOrder order)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Should to check whether the array is sorted in SortOrder order. An array should be not changed
        /// <exception cref="ArgumentNullException">Thrown when wrong parameter array</exception>
        /// </summary>
        public static bool IsSorted(int[] array, SortOrder order)
        {
            throw new NotImplementedException();
        }
    }

    
/*
    public enum SortOrder { Ascending, Descending };
    static public class OperationsWithArrays
    {
        public static void SortArray(int[] array, SortOrder order)
        {
            int tmp;
            int i, j;

            if (array == null)
                throw new ArgumentNullException();
            else
            {
                for (i = 0; i < array.Length - 1; i++)
                    for (j = 0; j < array.Length - 1 - i; j++)
                    {

                        if ((order == SortOrder.Ascending) && (array[j] > array[j + 1]) ||
                            (order == SortOrder.Descending) && (array[j] < array[j + 1]))
                        {
                            swap();
                        }
                    }

                void swap()
                {
                    tmp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = tmp;
                }
            }
        }
        public static bool IsSorted(int[] array, SortOrder order)
        {
            if (array == null)
                throw new ArgumentNullException();
            else
            {
                if (order == SortOrder.Ascending)
                {
                    for (int i = 0; i < array.Length - 1; i++)
                    {
                        if (array[i] > array[i + 1])
                            return false;
                    }
                    return true;
                }
                else
                {
                    for (int i = 0; i < array.Length - 1; i++)
                    {
                        if (array[i] < array[i + 1])
                            return false;
                    }
                    return true;
                }
            }
        }
    }
*/
}
