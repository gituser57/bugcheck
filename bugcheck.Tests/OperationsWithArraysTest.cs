using NUnit.Framework;
using System;

namespace bugcheck
{
    [TestFixture]
    public class OperationsWithArraysTest
    {
        /*[TestCase(null, SortOrder.Ascending)]
        [TestCase(null, SortOrder.Descending)]
        public void SortArray_NullArgumentArray_ThrowArgumentNullException(int[] array, SortOrder order)
        {
            Exception ex = Assert.Catch(() => OperationsWithArrays.SortArray(array, order));

            Assert.IsInstanceOf(typeof(ArgumentNullException), ex, "Array parameter is not checked on null");
        }

        [TestCase(null, SortOrder.Ascending)]
        [TestCase(null, SortOrder.Descending)]
        public void IsSorted_NullArgumentArray_ThrowArgumentNullException(int[] array, SortOrder order)
        {
            Exception ex = Assert.Catch(() => OperationsWithArrays.IsSorted(array, order));
            Assert.IsInstanceOf(typeof(ArgumentNullException), ex);
        }

        [TestCase(new int[] { 10, 20, 30 }, SortOrder.Ascending, true)]
        [TestCase(new int[] { 30, 20, 10 }, SortOrder.Descending, true)]
        [TestCase(new int[] { 30, 30, 30 }, SortOrder.Ascending, true)]
        [TestCase(new int[] { 30, 30, 30 }, SortOrder.Descending, true)]
        [TestCase(new int[] { 10, 20, 30 }, SortOrder.Descending, false)]
        [TestCase(new int[] { 30, 20, 10 }, SortOrder.Ascending, false)]
        [TestCase(new int[] { 30, -7, 30 }, SortOrder.Ascending, false)]
        [TestCase(new int[] { 90, 30, 100 }, SortOrder.Descending, false)]
        public void IsSorted_NotNullArraySorted_ResultReturned(int[] array, SortOrder order, bool result)
        {
            Assert.AreEqual(result, OperationsWithArrays.IsSorted(array, order));
        }

        [TestCase(new int[] { -3, 0, 0, 10, 20, 20, 30 }, new int[] { -3, 0, 0, 10, 20, 20, 30 }, SortOrder.Ascending)]
        [TestCase(new int[] { 30, 20, -3, 10, 20, 0, 0 }, new int[] { -3, 0, 0, 10, 20, 20, 30 }, SortOrder.Ascending)]
        [TestCase(new int[] { -100, -100, -100, -100 }, new int[] { -100, -100, -100, -100 }, SortOrder.Ascending)]
        [TestCase(new int[] { 0, 0, 0, 0 }, new int[] { 0, 0, 0, 0 }, SortOrder.Ascending)]
        [TestCase(new int[] { }, new int[] { }, SortOrder.Ascending)]
        public void SortArray_CheckAscending_ArraySortedAscending(int[] source, int[] expected, SortOrder order)
        {
            OperationsWithArrays.SortArray(source, order);

            CollectionAssert.AreEqual(expected, source, "Not sorted ascending correctly");
        }

        [TestCase(new int[] { 30, 20, 20, 10, 0, 0, -3 }, new int[] { 30, 20, 20, 10, 0, 0, -3 }, SortOrder.Descending)]
        [TestCase(new int[] { 30, 20, -3, 10, 20, 0, 0 }, new int[] { 30, 20, 20, 10, 0, 0, -3 }, SortOrder.Descending)]
        [TestCase(new int[] { -100, -100, -100, -100 }, new int[] { -100, -100, -100, -100 }, SortOrder.Descending)]
        [TestCase(new int[] { 0, 0, 0, 0 }, new int[] { 0, 0, 0, 0 }, SortOrder.Descending)]
        [TestCase(new int[] { }, new int[] { }, SortOrder.Descending)]
        public void SortArray_CheckDescending_ArraySortedDescending(int[] source, int[] expected, SortOrder order)
        {
            OperationsWithArrays.SortArray(source, order);

            CollectionAssert.AreEqual(expected, source, "Not sorted descending correctly");
        }
*/
        //[TestCase(new int[] { 10, 20, 30 }, SortOrder.Ascending, ExpectedResult = true)]
        //[TestCase(new int[] { 30, 20, 10 }, SortOrder.Descending, ExpectedResult = true)]
        //[TestCase(new int[] { 30, 30, 30 }, SortOrder.Ascending, ExpectedResult = true)]
        //[TestCase(new int[] { 30, 30, 30 }, SortOrder.Descending, ExpectedResult = true)]
        //[TestCase(new int[] { 10, 20, 30 }, SortOrder.Descending, ExpectedResult = false)]
        //[TestCase(new int[] { 30, 20, 10 }, SortOrder.Ascending, ExpectedResult = false)]
        //[TestCase(new int[] { 30, -7, 30 }, SortOrder.Ascending, ExpectedResult = false)]
        //[TestCase(new int[] { 90, 30, 100 }, SortOrder.Descending, ExpectedResult = false)]
        //public bool IsSorted_NotNullArraySorted_ResultReturned(int[] array, SortOrder order)
        //{
        //    return OperationsWithArrays.IsSorted(array, order);
        //}


    }
}
